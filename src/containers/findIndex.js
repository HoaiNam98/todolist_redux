export const findIndex = (taskArr, id) => {
  var result = -1
  taskArr.forEach((t, i) => {
    if (t.id === id) return (result = i)
  })
  return result
}

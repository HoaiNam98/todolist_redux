import {
  getTaskAPI,
  addTaskAPI,
  getUpdDataAPI,
  updTaskAPI,
  delTaskAPI
} from '../../services'

import {
  VIEW_TASK_SUCCESS,
  VIEW_TASK_ERROR,
  ADD_TASK_SUCCESS,
  ADD_TASK_ERROR,
  GET_UPD_DATA,
  GET_UPD_DATA_SUCCESS,
  GET_UPD_DATA_ERROR,
  UPD_TASK_SUCCESS,
  UPD_TASK_ERROR,
  DEL_TASK,
  DEL_TASK_SUCCESS,
  DEL_TASK_ERROR
} from '../constants'

export const viewTask = () => {
  return dispatch => {
    return getTaskAPI()
      .then(data => dispatch(viewTaskSuccess(data)))
      .catch(error => dispatch(viewTaskError(error)))
  }
}
export const viewTaskSuccess = data => {
  return {
    type: VIEW_TASK_SUCCESS,
    data
  }
}
export const viewTaskError = error => {
  return {
    type: VIEW_TASK_ERROR,
    error
  }
}

export const addTask = task => {
  return dispatch => {
    return addTaskAPI(task)
      .then(data => dispatch(addTaskSuccess(data)))
      .catch(err => dispatch(addTaskSuccess(err)))
  }
}

export const addTaskSuccess = data => {
  return {
    type: ADD_TASK_SUCCESS,
    data
  }
}

export const addTaskError = error => {
  return {
    type: ADD_TASK_ERROR,
    error
  }
}

export const getUpdData = id => {
  return dispatch => {
    return getUpdDataAPI(id)
      .then(data => dispatch(getUpdDataSuccess(data)))
      .catch(err => dispatch(getUpdDataError(err)))
  }
}

export const getUpdDataSuccess = updData => {
  return {
    type: GET_UPD_DATA_SUCCESS,
    updData
  }
}

export const getUpdDataError = error => {
  return {
    type: GET_UPD_DATA_ERROR,
    error
  }
}

export const updTask = newData => {
  return dispatch => {
    return updTaskAPI(newData)
      .then(data => dispatch(updTaskSuccess(data)))
      .catch(err => dispatch(updTaskError(err)))
  }
}

export const updTaskSuccess = data => {
  return {
    type: UPD_TASK_SUCCESS,
    data
  }
}

export const updTaskError = error => {
  return {
    type: UPD_TASK_ERROR,
    error
  }
}

export const delTask = id => {
  return dispatch => {
    return delTaskAPI(id)
      .then(data => {
        dispatch(delTaskSuccess(data))
      })
      .catch(err => dispatch(delTaskError(err)))
  }
}

const delTaskSuccess = data => {
  return {
    type: DEL_TASK_SUCCESS,
    data
  }
}
const delTaskError = error => {
  return {
    type: DEL_TASK_ERROR,
    error
  }
}

import { combineReducers } from 'redux'
import ToogleReducer from './toogleReducer'
import TaskReducer from './taskReducer';
export default combineReducers({
  ToogleReducer, TaskReducer
})

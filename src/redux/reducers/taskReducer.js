import {
  VIEW_TASK_SUCCESS,
  VIEW_TASK_ERROR,
  ADD_TASK_SUCCESS,
  ADD_TASK_ERROR,
  GET_UPD_DATA,
  GET_UPD_DATA_SUCCESS,
  GET_UPD_DATA_ERROR,
  UPD_TASK_SUCCESS,
  UPD_TASK_ERROR,
  DEL_TASK,
  DEL_TASK_SUCCESS,
  DEL_TASK_ERROR
} from '../constants'
import { findIndex } from '../../containers/findIndex'

const initialState = {
  loading: false,
  taskArr: [],
  message: null,
  updTask: null
}
export default (state = initialState, action) => {
  switch (action.type) {
    case VIEW_TASK_SUCCESS:
      return { ...state, loading: true, taskArr: action.data }
    case VIEW_TASK_ERROR:
      return { ...state, loading: false, message: action.error }

    case ADD_TASK_SUCCESS:
      console.log(action.data)
      return { ...state, taskArr: action.data }
    case ADD_TASK_ERROR:
      console.log(action.error)
      return { ...state, message: action.error }

    case GET_UPD_DATA_SUCCESS:
      return { ...state, updTask: action.updData }
    case GET_UPD_DATA_ERROR:
      return { ...state, message: action.error }

    case UPD_TASK_SUCCESS:
      console.log(action.data)
      return { ...state, taskArr: action.data, updTask: null }
    case UPD_TASK_ERROR:
      return { ...state, message: action.error }

    case DEL_TASK_SUCCESS:
      return { ...state, taskArr: action.data }
    case DEL_TASK_ERROR:
      return { ...state, message: action.error }
    default:
      return state
  }
}

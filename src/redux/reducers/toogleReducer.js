import { TOOGLE } from '../actions/toogleAction'
const initialState = false

export default (state = initialState, action) => {
  if (action.type === TOOGLE) {
    state = !state
  }
  return state
}

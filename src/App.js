import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import AddJob from './components/addTask.jsx'
import ViewJob from './components/viewTask.jsx'
import { openForm } from './redux/actions/toogleAction'
import { viewTask } from './redux/actions/taskAction'
import './App.css'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import Home from './components/home'
import About from './components/about'


// import {get_product_list} from './redux/actions/index'
// import {useDispatch,useSelector} from 'react-redux'

function App () {
  const dispatch = useDispatch()
  const data = useSelector(state => state)
  const { TaskReducer } = useSelector(state => state)
  const disPatchToogle = e => {
    e.preventDefault()
    dispatch(openForm())
  }
  const getTask = e => {
    e.preventDefault()
    dispatch(viewTask())
  }
  // useEffect(() => {
  //   console.log(TaskReducer)
  // }, [TaskReducer])
  return (

     
   

    <Router>
      <div className='App'>
        <div className='container'>
          <h1>Quản lý công việc</h1>
          <button onClick={e => disPatchToogle(e)}>Thêm công việc</button>
          <hr />
          <button onClick={e => getTask(e)}>View task</button>
          <hr />
          <div className='row'>
            <div className={data.ToogleReducer ? 'col-lg-4' : 'd-none'}>
              <AddJob />
            </div>
            <div className={data.ToogleReducer ? 'col-lg-8' : 'col-12'}>
              <ViewJob />
            </div>
          </div>
        </div>
      </div>
      {/*  */}
      <div>
        <nav>
          <ul>
            <li>
              <Link to='/'>Home</Link>
            </li>
            <li>
              <Link to='/about'>About</Link>
            </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path='/about'>
            <About />
          </Route>
          <Route path='/'>
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  )
}

export default App

import { findIndex } from '../containers/findIndex'

export const getTaskAPI = async () => {
  let local = await localStorage.getItem('taskArr')
  let Arr = local ? await JSON.parse(local) : []
  return Arr
}

export const addTaskAPI = async task => {
  let local = await localStorage.getItem('taskArr')
  let Arr = local ? await JSON.parse(local) : []
  if (task.id === null) task.id = Arr.length + 1
  Arr = [...Arr, task]
  localStorage.setItem('taskArr', JSON.stringify(Arr))
  return Arr
}

export const updTaskAPI = async data => {
  let Arr = await JSON.parse(localStorage.getItem('taskArr'))
  let newArr = Arr.map((t, i) => {
    if (data.id === t.id) {
      t.name = data.name
      t.status = data.status
    }
    return t
  })
  await localStorage.setItem('taskArr', JSON.stringify(newArr))
  // console.log(Arr)
  return newArr
}

export const getUpdDataAPI = async id => {
  let Arr = await JSON.parse(localStorage.getItem('taskArr'))
  let index = await findIndex(Arr, id)
  let updData = Arr[index]
  return updData
}

export const delTaskAPI = async(id) =>{
  let Arr = await JSON.parse(localStorage.getItem('taskArr'))
  let newArr = Arr.filter(t => t.id !== id)
  await localStorage.setItem('taskArr', JSON.stringify(newArr))
  return newArr
}

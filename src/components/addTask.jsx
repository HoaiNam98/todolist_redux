import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { addTask, updTask } from '../redux/actions/taskAction.js'
// import {useDispatch} from 'react-redux'
// import {add_product} from '../redux/actions/index.js'
const AddJob = ({ jobs, updJob }) => {
  const [taskItem, setTaskItem] = useState({
    id: null,
    name: '',
    status: 1
  })
  const dispatch = useDispatch()
  const { TaskReducer } = useSelector(state => state)
  const onSubmit = event => {
    event.preventDefault()
    taskItem.id === null
      ? dispatch(addTask(taskItem))
      : dispatch(updTask(taskItem))
    setTaskItem({
      id: null,
      name: '',
      status: 1
    })
  }
  useEffect(() => {
    if (TaskReducer.updTask !== null) {
      let updData = TaskReducer.updTask
      setTaskItem({
        id: updData.id,
        name: updData.name,
        status: updData.status
      })
    }
  }, [TaskReducer])
  return (
    <div className='panel panel-warning'>
      <div className='panel-heading'>
        <h3 className='panel-title'>
          {taskItem.id === null ? 'Thêm công việc' : 'Cập nhật công việc'}
        </h3>
      </div>
      <div className='panel-body'>
        <form>
          <div className='form-group'>
            <label>Tên :</label>
            <input
              value={taskItem.name}
              type='text'
              className='form-control'
              onChange={event =>
                setTaskItem({ ...taskItem, name: event.target.value })
              }
            />
          </div>
          <label>Trạng Thái :</label>
          <select
            value={taskItem.status}
            className='form-control'
            required='required'
            onChange={event =>
              setTaskItem({ ...taskItem, status: event.target.value })
            }
          >
            <option value='1'>Kích Hoạt</option>
            <option value='0'>Ẩn</option>
          </select>
          <br />
          <div className='text-center'>
            <button
              type='button'
              className='btn btn-warning'
              onClick={e => onSubmit(e)}
            >
              Thêm
            </button>
            &nbsp;
            <button className='btn btn-danger'>Hủy Bỏ</button>
          </div>
        </form>
      </div>
    </div>
  )
}

export default AddJob

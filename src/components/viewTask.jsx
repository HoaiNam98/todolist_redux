import React from 'react'
import Items from './taskItem'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { viewTask } from '../redux/actions/taskAction.js'
const ViewJob = ({ jobList, delJob, updStt, keywords, updItem }) => {
  const dispatch = useDispatch()
  const { taskArr } = useSelector(state => state.TaskReducer)
  useEffect(() => {
    dispatch(viewTask())
  }, [dispatch])

  useEffect(() => {
    console.log(taskArr);
  }, [taskArr])

  const taskListComponent = taskArr.map((t, i) => {
    return <Items key={i} data={t} />
  })
  return (
    <div>
      <table className='table table-bordered table-hover'>
        <thead>
          <tr>
            <th className='text-center'>STT</th>
            <th className='text-center'>Tên</th>
            <th className='text-center'>Trạng Thái</th>
            <th className='text-center'>Hành Động</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td></td>
            <td>
              <input
                type='text'
                className='form-control'
                onChange={e => keywords(e.target.value)}
              />
            </td>
            <td>
              <select className='form-control'>
                <option value='-1'>Tất Cả</option>
                <option value='0'>Ẩn</option>
                <option value='1'>Kích Hoạt</option>
              </select>
            </td>
            <td></td>
          </tr>
          {taskListComponent}
        </tbody>
      </table>
    </div>
  )
}

export default ViewJob

import React from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {getUpdData, delTask} from '../redux/actions/taskAction.js'
const ItemList = ({ data }) => {
  const dispatch = useDispatch()
  return (
    <tr>
      <td>{data.id}</td>
      <td>{data.name}</td>
      <td className='text-center'>
        <span
          style={{
            border:
              data.status === 1 ? '1px solid #5cb85c' : '1px solid #c2bfbf',
            backgroundColor: Number(data.status) === 1 ? '#5cb85c' : '#c2bfbf',
            borderRadius: '5px',
            padding: '0 10px',
            color: 'white',
            fontWeight: '600'
          }}
          // onClick={() => updSttId(data.id)}
        >
          {Number(data.status) === 1 ? 'Kích Hoạt' : 'Ẩn'}
        </span>
      </td>
      <td className='text-center'>
        <button
          type='button'
          className='btn btn-warning'
          onClick={() => dispatch(getUpdData(data.id))}
        >
          <span className='fa fa-pencil mr-4'></span>Sửa
        </button>
        &nbsp;
        <button
          id={`${data.id}`}
          type='button'
          className='btn btn-danger'
          onClick={() => dispatch(delTask(data.id))}
        >
          <span className='fa fa-trash mr-4'></span>Xóa
        </button>
      </td>
    </tr>
  )
}

export default ItemList
